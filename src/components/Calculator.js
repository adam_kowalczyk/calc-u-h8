import React, { useEffect, useState } from 'react';
import './Calculator.css';

export default () => {
  const maxDisplayLength = 16;
  const operatorsMap = {
    '+': (a, b) => a + b,
    '-': (a, b) => a - b,
    '*': (a, b) => a * b,
    '/': (a, b) => a / b,
  };
  const operators = Object.keys(operatorsMap);

  // Is the calculator in an error state?
  const [err, setErr] = useState(false);
  // Should the next value input overwrite the current value?
  const [overwriteValue, setOverwriteValue] = useState(false);
  // The current display value
  const [value, setValue] = useState('');
  // The current operator
  const [currentOperator, setCurrentOperator] = useState('');
  // The stack to be resolved on equals call
  const [stack, setStack] = useState([]);

  const clear = (clearStack) => {
    setValue('');
    setOverwriteValue(false);
    if (clearStack) {
      setStack([]);
      setErr(false);
      setCurrentOperator('');
    }
  };

  const changeValue = (number) => {
    if (overwriteValue) {
      // We are still displaying a value that has already been pushed to the stack,
      // so must overwrite the current value rather than append to it
      setValue(String(number));
      setOverwriteValue(false);
      setCurrentOperator('');
    } else if (value.length >= maxDisplayLength - 1) {
      // Do not allow the value length to exceed the max allowed length
      return;
    } else {
      setValue(`${value}${number}`);
    }
  }

  const negate = () => {
    setValue(String(Number(value) * -1));
  }

  const setOperator = (operator) => {
    if (overwriteValue) {
      // The current value has already been pushed to the stack
      // so only the operator must be added
      setStack([...stack, operator]);
    } else {
      setStack([...stack, Number(value), operator]);
      setOverwriteValue(true);
    }
    setCurrentOperator(operator);
  }

  const equal = () => {
    let operation;
    const fullStack = [...stack, Number(value)];
    const result = fullStack.reduce((acc, val) => {
      if (operators.includes(val)) {
        // in the case of successive operators, the last in the sequence will be respected
        operation = operatorsMap[val].bind(null, acc);
        return acc;
      } else if (operation) {
        return operation(val);
      }
      // this shouldn't happen in normal operation
      setErr(true);
      return null;
    });
    setValue(String(result));
    setStack([]);
    setCurrentOperator('');
  }

  const handleKeyDown = (event) => {
    const key = event.key;
    if (operators.includes(key)) {
      setOperator(key);
    } else if (/\d|\./.test(key)) {
      changeValue(key);
    } else if (['Esc', 'Escape'].includes(key)) {
      clear(true);
    } else if (['Backspace', 'Delete'].includes(key)) {
      clear();
    } else if (['=', 'Return', 'Enter'].includes(key)) {
      equal();
    } else if (['!', '±'].includes(key)) {
      negate();
    }
  }

  useEffect(() => {
    document.addEventListener('keydown', handleKeyDown, false);
    return function cleanup() {
      document.removeEventListener('keydown', handleKeyDown, false);
    };
  });

  const renderValueButtons = (values) => {
    return values.map((val, i) => {
      return <div key={`${i}-${val}`} onClick={changeValue.bind(null, val)}>{val}</div>;
    });
  }

  const renderOperatorButton = (operator) => {
    return <div onClick={setOperator.bind(null, operator)}>{operator}</div>;
  }

  // Any good pocket calculator shows an error when the number gets too long for the display...
  if (!err && (Math.abs(value.length) > maxDisplayLength || /e/.test(value))) {
    // since this isn't a very good calcuator, we don't allow exponent notation either!
    setErr(true);
  }
  const diplayValue = err  ? 'E' : value;

  return (
    <div className='Calc' onKeyDown={handleKeyDown}>
      <div className='Calc-Screen' data-operator={currentOperator}>{diplayValue}</div>
      <div className='Calc-Row'>
        <div className='Calc-Column'>
          <div className='Calc-Row'>
            <div onClick={clear.bind(null, true)}>AC</div>
            <div onClick={clear}>C</div>
            <div onClick={negate}>+/-</div>
          </div>
          <div className='Calc-Row'>
            {renderValueButtons([7,8,9])}
          </div>
          <div className='Calc-Row'>
            {renderValueButtons([4,5,6])}
          </div>
          <div className='Calc-Row'>
            {renderValueButtons([1,2,3])}
          </div>
          <div className='Calc-Row'>
            {renderValueButtons([0, '.'])}
            <div onClick={equal}>=</div>
          </div>
        </div>
        <div className='Calc-Column'>
          <div className='Calc-Row'>
            {renderOperatorButton('/')}
          </div>
          <div className='Calc-Row'>
            {renderOperatorButton('*')}
          </div>
          <div className='Calc-Row'>
            {renderOperatorButton('-')}
          </div>
          <div className='Calc-Row Calc-Row--tall'>
            {renderOperatorButton('+')}
          </div>
        </div>
      </div>
    </div>
  );
};
